import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NgOpenlayersModule} from 'ng-openlayers-lib';
import { Epsg21781Component } from './epsg21781/epsg21781.component';
import { OsmWithControlsComponent } from './osm-with-controls/osm-with-controls.component';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import { LayersControlComponent } from './layers-control/layers-control.component';
import {EventsComponent} from './events/events.component';
import {FormsModule} from '@angular/forms';
import { Epsg28992Component } from './epsg28992/epsg28992.component';

export const routes: Routes = [
  {
    path: 'osm',
    component: OsmWithControlsComponent
  },
  {
    path: 'events',
    component: EventsComponent
  },
  {
    path: 'epsg21781',
    component: Epsg21781Component
  },
  {
    path: 'epsg28992',
    component: Epsg28992Component
  },
  {
    path: 'layers-control',
    component: LayersControlComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    Epsg21781Component,
    Epsg28992Component,
    EventsComponent,
    OsmWithControlsComponent,
    LayersControlComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    NgOpenlayersModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
