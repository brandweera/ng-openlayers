import {Component} from '@angular/core';
import * as ol from 'openlayers';

@Component({
  selector: 'app-epsg21781',
  templateUrl: './epsg21781.component.html'
})
export class Epsg21781Component {
  center = [660000, 190000];
  zoom = 9;
  projection = new ol.proj.Projection({
    code: 'EPSG:21781',
    units: 'm'
  });
  layers = [
    new ol.layer.Image({
      title: 'paerke_nationaler_bedeutung',
      source: new ol.source.ImageWMS({
        params: {'LAYERS': 'ch.bafu.schutzgebiete-paerke_nationaler_bedeutung'},
        serverType: 'mapserver',
        url: 'https://wms.geo.admin.ch/'
      })
    }),
    new ol.layer.Tile({
      title: 'swisstopo.pixelkarte-farbe-pk1000',
      opacity: 0.5,
      source: new ol.source.TileWMS({
        params: {
          'LAYERS': 'ch.swisstopo.pixelkarte-farbe-pk1000.noscale',
          'FORMAT': 'image/jpeg'
        },
        url: 'https://wms.geo.admin.ch/'
      })
    })
  ];

  constructor() {
  }

}
