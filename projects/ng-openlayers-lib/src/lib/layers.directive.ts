import {Directive, Input, IterableDiffer, IterableDiffers} from '@angular/core';
import {layer, Map} from 'openlayers';
import {MapComponent} from './map.component';

@Directive({
  selector: 'ol-map > [olLayers]'
})
export class LayersDirective {

  private differ: IterableDiffer<any>;

  constructor(private mapComponent: MapComponent,
              private differs: IterableDiffers) {
    this.differ = differs.find([]).create(null);
  }

  @Input()
  set olLayers(value: layer.Layer[]) {
    if (value) {
      const changes = this.differ.diff(value);
      if (changes) {
        this.updateLayers(changes, value);
      }
    }
  }

  private updateLayers(changes: any, layers: layer.Layer[]) {

    if (changes) {
      const map = this.mapComponent.getMap();
      changes.forEachAddedItem((change) => {
        this.addLayer(map, change.item);
      });
      changes.forEachRemovedItem((change) => {
        this.removeLayer(map, change.item);
      });

      layers.forEach((item, index) => {
        const length = map.getLayers().getLength() - 1;
        item.setZIndex(length - index);
      });
    }
  }

  private addLayer(map: Map, olLayer: layer.Layer, animate = true) {
    map.addLayer(olLayer);
  }

  private removeLayer(map: Map, olLayer: layer.Layer, animate = true) {
    map.addLayer(olLayer);
  }

}
