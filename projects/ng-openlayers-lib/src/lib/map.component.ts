import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {control, interaction, layer, View, Map, MapBrowserEvent, MapEvent, source} from 'openlayers';
import {MapService} from './map.service';

@Component({
  selector: 'ol-map',
  styleUrls: ['./map.component.scss'],
  template: '<div style="width: 100%; height: 100%; margin: 0; padding: 0;"></div><ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapComponent implements OnInit, AfterViewInit {
  /**
   * This event is triggered after the map is initialized
   * Use this to have access to the maps and some helper functions
   */
  @Output()
  mapReady: EventEmitter<any> = new EventEmitter();

  /**
   * This event is triggered after the user clicks on the map.
   * A true single click with no dragging and no double click.
   * Note that this event is delayed by 250 ms to ensure that it is not a double click.
   */
  @Output()
  mapClick: EventEmitter<MapBrowserEvent> = new EventEmitter<MapBrowserEvent>();

  @Input() pixelRatio: number;
  @Input() keyboardEventTarget: Element | string;
  @Input() loadTilesWhileAnimating: true;
  @Input() loadTilesWhileInteracting: true;
  @Input() logo: string | boolean;
  @Input() renderer: 'canvas' | 'webgl';

  // we pass empty arrays to not get default controls/interactions because we have our own directives
  controls: control.Control[] = [];
  interactions: interaction.Interaction[] = [];

  private map: Map;
  private timeoutId: any;

  constructor(private element: ElementRef,
              private mapService: MapService) {
  }

  ngOnInit() {
    const target = this.element.nativeElement.firstElementChild;
    this.map = new Map(this);
    this.map.setTarget(target);
    // register the map in the injectable mapService
    this.mapService.addMap(this.map);

    this.map.once('postrender', event => {
      this.afterMapReady();
    });

  }

  ngAfterViewInit() {

    // Add default controls if not defined
    if (this.map.getControls().getLength() === 0) {
      this.map.addControl(new control.Zoom);
      // this.map.addControl(new control.ScaleLine);
    }

    // Add default interactions if not defined
    if (this.map.getInteractions().getLength() === 0) {
      this.map.addInteraction(new interaction.DoubleClickZoom);
      this.map.addInteraction(new interaction.DragPan);
      this.map.addInteraction(new interaction.PinchZoom);
      this.map.addInteraction(new interaction.MouseWheelZoom({
        constrainResolution: true // force zooming to a integer zoom
      }));
      this.map.addInteraction(new interaction.DragZoom);
    }
  }

  afterMapReady() {
    // register map events
    this.map.on('singleclick', (event: ol.MapBrowserEvent) => setTimeout(this.mapClick.emit(event), 20));

    // react on window resize
    window.addEventListener('resize', () => this.update());

    setTimeout(this.mapReady.emit({map: this.map, mapService: this.mapService}));
    this.map.updateSize();
  }

  getMap() {
    return this.map;
  }

  public update() {
    clearTimeout(this.timeoutId);
    this.timeoutId = setTimeout(() => {
      this.map.updateSize();
      this.map.renderSync();
    }, 100);
  }

}
