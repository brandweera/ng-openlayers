import { Component, ContentChild, Input, OnDestroy, OnInit } from '@angular/core';
import { Overlay, OverlayPositioning, Coordinate } from 'openlayers';
import {ContentComponent} from './content.component';
import {MapComponent} from './map.component';

@Component({
  selector: 'ol-map > [olOverlay]',
  template: '<ng-content></ng-content>'
})
export class OverlayComponent implements OnInit, OnDestroy {
  componentType = 'overlay';
  overlay: Overlay;
  element: Element;
  @ContentChild(ContentComponent) content: ContentComponent;

  @Input() id: number|string;
  @Input() offset: number[];
  @Input() positioning: OverlayPositioning|string;
  @Input() stopEvent: boolean;
  @Input() insertFirst: boolean;
  @Input() autoPan: boolean;
  @Input() autoPanAnimation: any;
  @Input() autoPanMargin: number;
  @Input() coordinate: Coordinate;

  constructor(
    private mapComponent: MapComponent
  ) {
  }

  ngOnInit() {
    if (this.content) {
      this.element = this.content.elementRef.nativeElement;
      this.overlay = new Overlay(this);
      this.mapComponent.getMap().addOverlay(this.overlay);
    }
  }

  ngOnDestroy() {
    if (this.overlay) {
      this.mapComponent.getMap().removeOverlay(this.overlay);
    }
  }
}
