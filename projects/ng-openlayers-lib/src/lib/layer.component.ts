import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {Extent, layer, source} from 'openlayers';
import {MapComponent} from './map.component';
import {LayerType, SourceType} from './models';
import {osmLayer, osmSource} from './util';

@Component({
  selector: 'ol-map > ol-layer',
  template: '<ng-content></ng-content>'
})
export class LayerComponent implements OnInit, OnDestroy, OnChanges {
  private layer: layer.Layer;

  @Input() layerType: LayerType;
  @Input() sourceType: SourceType;
  @Input() opacity: number;
  @Input() visible: boolean;
  @Input() extent: Extent;
  @Input() zIndex: number;
  @Input() minResolution: number;
  @Input() maxResolution: number;
  @Input() hidpi: boolean;
  @Input() sourceOptions: any;
  @Input() properties: { [index: string]: any };

  source: any;

  constructor(private mapComponent: MapComponent) {
  }

  ngOnInit() {

    switch (this.layerType) {
      case LayerType.IMAGE:
        this.source = new source.ImageWMS(this.sourceOptions);
        this.layer = new layer.Image(this);
        break;
      case LayerType.TILE:
        this.source = this.getTileSource(this.sourceOptions);
        this.layer = new layer.Tile(this);
        break;
      case LayerType.VECTOR_TILE:
        this.source = new source.VectorTile(this.sourceOptions);
        this.layer = new layer.VectorTile(this);
        break;
      case LayerType.VECTOR:
        this.source = new source.Vector(this.sourceOptions);
        this.layer = new layer.Vector(this);
        break;
      default:
        this.layer = osmLayer();
        break;
    }

    const map = this.mapComponent.getMap();
    if (this.layer && map) {
      this.layer.setZIndex(map.getLayers().getLength());
      map.addLayer(this.layer);
    }

  }

  ngOnChanges(changes: SimpleChanges) {
    const properties: { [index: string]: any } = {};
    if (!this.layer) {
      return;
    }

    for (const key in changes) {
      properties[key] = changes[key].currentValue;
    }
    this.layer.setProperties(properties, false);
  }

  ngOnDestroy() {
    const map = this.mapComponent.getMap();
    if (this.layer && map) {
      map.removeLayer(this.layer);
    }
  }

  getUrl(): string {
    if (this.source instanceof source.ImageWMS) {
      return this.source.getUrl();
    }
    if (this.source instanceof source.WMTS) {
      return this.source.getUrls()[0];
    }
    if (this.source instanceof source.TileWMS) {
      return this.source.getUrls()[0];
    } else {
      return '';
    }
  }

  updateParams(name: string, value: any) {
    const params = this.source.getParams();
    params[name.toUpperCase()] = value;
    params['t'] = new Date().getMilliseconds();
    this.source.updateParams(params); // {'TIME': startDate.toISOString()}
  }

  private getTileSource(sourceOptions: any) {

    switch (sourceOptions.sourceType) {
      case SourceType.TILEWMS:
        return new source.TileWMS(sourceOptions);
      case SourceType.TILEIMAGE:
        return new source.TileImage(sourceOptions);
      case SourceType.WMTS:
        return new source.WMTS(sourceOptions);
      case SourceType.XYZ:
        return new source.XYZ(sourceOptions);
      default:
        return osmSource();
    }
  }

}
