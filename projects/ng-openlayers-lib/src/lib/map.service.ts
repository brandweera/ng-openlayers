import { Injectable } from '@angular/core';
import {Map, proj} from 'openlayers';
import proj4 from 'proj4';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  maps: Map[];

  constructor() {
    this.maps = [];
  }

  /**
   * Retrieves all the maps
   */
  getMaps(): Map[] {
    return this.maps;
  }

  /**
   * Returns a map object from the maps array
   */
  getMapById(id: string): Map {
    let map: Map = null;
    for (let i = 0; i < this.maps.length; i++) {
      if (this.maps[i].getTarget() === id) {
        map = this.maps[i];
        break;
      }
    }
    return map;
  }

  addMap(map: Map): void {
    this.maps.push(map);
  }

  updateSize() {
    this.maps.forEach(map => {
      map.updateSize();
    });
  }

  addProjection(projectionOptions: any) {
    // A minimal projection object is configured with only the SRS code and the map
    // units. No client-side coordinate transforms are possible with such a
    // projection object. Requesting tiles only needs the code together with a
    // tile grid of Cartesian coordinates; it does not matter how those
    // coordinates relate to latitude or longitude.
    // let proj = new ol.proj.Projection({
    //   code: epsg,
    //   extent: [-285401.92, 22598.08, 595401.9199999999, 903401.9199999999],
    //   worldExtent: [3.2, 50.75, 7.22, 53.7],
    //   metersPerUnit: 1.0,
    //   units: "m"
    // });
    // ol.proj.addProjection(proj);
    proj.addProjection(new proj.Projection(projectionOptions));
  }

  addProj4(epsg: string, proj4Def: string, extent?: any) {
    console.log(`addProj4 ${epsg}...`);
    let projection = proj.get(epsg);
    if (!projection) {
      console.log(`Registering ${epsg} in OpenLayers...`);
      proj.setProj4(proj4);
      proj4.defs(epsg, proj4Def);
      projection = proj.get(epsg);
      if (extent) {
        console.log(`Applying extent for ${epsg} in OpenLayers`);
        projection.setExtent(extent);
      }

    }
    if (!projection) {
      console.error(`Failed to register ${epsg} projection in OpenLayers`);
    }
    proj.addProjection(projection);
  }

}
