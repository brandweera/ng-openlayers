import {layer, source} from 'openlayers';

export function stringToEl(html: string) {
  const parser = new DOMParser();
  const DOM = parser.parseFromString(html, 'text/html');
  return DOM.body.firstChild;
}

export function requestInterval(fn, delay) {
  if (!window.requestAnimationFrame &&
    !window.webkitRequestAnimationFrame)
    return window.setInterval(fn, delay);

  let start = new Date().getTime();
  const handle: any = {};

  function loop() {
    handle.value = requestAnimationFrame(loop);
    const current = new Date().getTime(),
      delta = current - start;
    if (delta >= delay) {
      fn.call();
      start = new Date().getTime();
    }
  }

  handle.value = requestAnimationFrame(loop);
  return handle;
}

export function clearRequestInterval(handle) {
  window.cancelAnimationFrame ? window.cancelAnimationFrame(handle.value) :
    window.webkitCancelAnimationFrame ? window.webkitCancelAnimationFrame(handle.value) :
      clearInterval(handle);
}


export function defaultLayers() {
  return [
    testTileLayer(),
    osmLayer()
  ];
}

export function osmLayer() {
  return new layer.Tile({
    source: new source.OSM()
  });
}

export function osmSource() {
  return new source.OSM();
}

export function testTileLayer() {
  return new layer.Tile({
    opacity: 0.8,
    extent: [-13884991, 2870341, -7455066, 6338219],
    source: new source.TileWMS({
      url: 'https://ahocevar.com/geoserver/wms',
      params: {'LAYERS': 'ne:NE1_HR_LC_SR_W_DR', 'TILED': true}
    })
  });
}

export function brtLayers() {

}



