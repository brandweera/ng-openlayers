import {Directive, Input} from '@angular/core';
import {control} from 'openlayers';
import {MapComponent} from './map.component';
import {stringToEl} from './util';

@Directive({
  selector: 'ol-map > [olControls]'
})
export class ControlsDirective {

  private readonly controlList = {
    attribution: control.Attribution,
    fullscreen: control.FullScreen,
    mouseposition: control.MousePosition,
    rotate: control.Rotate,
    scaleline: control.ScaleLine,
    zoom: control.Zoom,
    zoomslider: control.ZoomSlider,
    zoomtoextent: control.ZoomToExtent
  };

  constructor(private mapComponent: MapComponent) {
  }

  @Input()
  set olControls(value: any[]) {
    const map = this.mapComponent.getMap();
    if (undefined !== map) {
      map.getControls().clear();
      for (const config of value) {
        this.addControl(map, config);
      }
    }
  }

  private addControl(map, controlConfig) {
    if (!this.controlList[controlConfig.name]) {
      console.error(`Unknown control ${controlConfig.name}`);
      return;
    }
    this.checkLabels(controlConfig.options);
    const newControl = new this.controlList[controlConfig.name](controlConfig.options);
    map.addControl(newControl);
  }

  private checkLabels(options) {
    if (!options) {
      return;
    }

    options.forEach(function (element) {
      const value = element;
      if (typeof value === 'string' && value.startsWith('<span') && value.endsWith('span>')) {
        element = stringToEl(value);
      }
    });
  }

}
